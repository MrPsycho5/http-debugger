#!/usr/bin/python3
from fastapi import FastAPI, Request, Response
import json

import uvicorn

app = FastAPI()

jsonFileName = "requests.json"

def appendRequest(request):
    requests = []
    with open(jsonFileName, "r") as jsonFile:
        requests = json.load(jsonFile)
    requests.append(request)
    with open(jsonFileName, "w") as jsonFile:
        json.dump(requests, jsonFile)

def readRequests():
    requests = []
    with open(jsonFileName, "r") as jsonFile:
        requests = json.load(jsonFile)
    return requests

def createRequestsFile():
    try:
        with open(jsonFileName, "r") as jsonFile:
            json.load(jsonFile)
    except:
        with open(jsonFileName, "w") as jsonFile:
            json.dump([], jsonFile)

def clearRequestsFile():
    with open(jsonFileName, "w") as jsonFile:
        json.dump([], jsonFile)

@app.get("/logs")
async def getLogs(request: Request, response: Response):
    response.headers["Content-Type"] = "application/json"
    return readRequests()

@app.get("/clear")
async def root():
    clearRequestsFile()
    return "Cleared requests file."

@app.api_route("/{path_name:path}")
async def catch_all(request: Request, path_name: str):
    method = request.method
    headers = request.headers
    body = await request.body()

    log = {"method": method, "path_name": path_name, "headers": str(headers), "body": body.decode("utf-8")}
    appendRequest(log)

    return {"request_method": request.method, "path_name": path_name}

if __name__ == "__main__":
    createRequestsFile()
    uvicorn.run(app, port=8000)